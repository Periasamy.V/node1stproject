import { Component } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent {
  // String Interpolation 
 name="sam";
// Property Binding
 click=false

// Event Binding
 result=''
 onSubmit(event:any){
  this.result=(<HTMLInputElement>event.target).value

 }

//  Two Way Binding
 serve="sam"






}
